package com.envodige;

import java.util.*;

class LanguageStudent {
    List<String> langs = new ArrayList<>();
    public Collection<String> getLanguages() {
        return langs;
    }

    public void addLanguage(String lang) {
        langs.add(lang);
    }

}

class LanguageTeacher extends  LanguageStudent{

    boolean teach(LanguageStudent student, String lang) {
        if( getLanguages().contains(lang)) {
            student.addLanguage(lang);
            return true;
        }
        return false;
    }
    public static void main(String[] args) {
      LanguageTeacher teacher = new LanguageTeacher();
      teacher.addLanguage("English");

      LanguageStudent student = new LanguageStudent();
      teacher.teach(student, "English");

      for(String language : student.getLanguages())
          System.out.println(language);
    }
}
