package test;

public class Main
{
    public static void aMethod() throws Exception {
        try {
            throw new Exception();
        }
        finally {
            System.out.println("finally");
            return;
        }
    }

    public static void main(String[] args) {
        try{
            aMethod();
        }catch (Exception e)
        {
            System.out.println("exception");
            return;
        }
        System.out.println("finished");
    }
}