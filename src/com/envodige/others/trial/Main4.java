package test;

import java.util.Arrays;
import java.util.stream.Stream;

public class Main4 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5};

        long count = Arrays.stream(arr).filter(x -> x < 3).count();
        long xx = Stream.of(arr).flatMapToInt(x -> Arrays.stream(x)).count();
        System.out.println(xx);
    }
}
