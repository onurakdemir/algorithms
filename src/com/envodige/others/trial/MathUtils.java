package test;
public class MathUtils {
    public static double average(int a, int b) {
        return (a + b) / (double)2;
    }

    public static void others() {
        // Use of floor method
        double f1 = 30.56, f2 = -56.34;
        f1 =Math.floor(f1);
        System.out.println("Floor value of f1 : "+f1);

        f2 =Math.floor(f2);
        System.out.println("Floor value of f2 : "+f2);
        System.out.println("");

        // Use of hypot() method
        double p = 12, b = -5;
        double h = Math.hypot(p, b);
        System.out.println("Hypotenuse : "+h);
        System.out.println("");

        // Use of IEEEremainder() method
        double d1 = 105, d2 = 2;
        double r = Math.IEEEremainder(d1,d2);
        System.out.println("Remainder : "+r);
        System.out.println("");

        // Use of log() method
        double l = 10;
        l = Math.log(l);
        System.out.println("Log value of 10 : "+l);

    }

    public static void main(String[] args) {
        System.out.println(average(2,1));
        others();
    }
}
