package test;

public class ExampleFindLowerCase {

    public static void main(String[] args) {
        String path = "AbqqRTLsfMN";
        String lowerCasePath = path.toUpperCase();
        int count = 0;
        for(int i = 0; i < path.length(); i++) {
            if( path.charAt(i) == lowerCasePath.charAt(i)) {
                count++;
            }
        }

        System.out.println(count);
    }

}
