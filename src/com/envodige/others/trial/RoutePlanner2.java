package test;

public class RoutePlanner2 {

    public static boolean routeFound = false;

    public static void DFSUtil(boolean[][] grid, int row, int col, int toRow, int toCol, boolean[][] visited) {

        int H = grid.length;
        int L = grid[0].length;

        if (row < 0 || col < 0 || row >= H || col >= L || visited[row][col])
            return;

        if(row == toRow && col == toCol) {
            routeFound = true;
        }

        //mark the cell visited
        visited[row][col] = true;
        //System.out.print(grid[row][col] + " ");
        DFSUtil(grid, row+ 1, col,toRow, toCol,visited); // go right
        DFSUtil(grid, row- 1, col,toRow, toCol,visited); //go left
        DFSUtil(grid, row, col + 1,toRow, toCol,visited); //go down
        DFSUtil(grid, row, col - 1,toRow, toCol,visited); // go up
    }


    public static boolean routeExists(int fromRow, int fromColumn, int toRow, int toColumn,
                                      boolean[][] mapMatrix) {
        int h = mapMatrix.length;
        if (h == 0)
            return false;
        int l = mapMatrix[0].length;

        //created visited array
        boolean [][] visited = new boolean[h][l];
        //System.out.println("Depth-First Search: ");
        DFSUtil(mapMatrix, fromRow,  fromColumn,toRow, toColumn, visited);
        return routeFound;
    }

    public static void main(String[] args) {
        boolean[][] mapMatrix = {
                {true,  false, false},
                {true,  true,  false},
                {false, true,  true}
        };

        System.out.println(routeExists(0, 0, 2, 2, mapMatrix));
    }
}