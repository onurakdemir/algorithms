package test;

public class Main5 {
    public static void main(String[] args) {
        double a = 10;
        System.out.println(a / 0);

        System.out.println(new Double(a/0).compareTo(Double.POSITIVE_INFINITY) == 0);
    }
}
