package test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class TwoSumAll {
    public static void findTwoSum(int[] list, int sum) {
        if (list == null || list.length < 2 || sum < 1)
            return;
        //map values to indexes
        final Map<Integer, Integer> indexMap = new HashMap<>();
        indexMap.put(list[0], 0);
        int nextLocation = 1;
        for (int i = 1; i < list.length; i++) {
            int needed = sum - list[i];
            if (indexMap.get(needed) == null) {
                indexMap.put(list[nextLocation++], 0);
            } else {
                indexMap.put(needed, list[nextLocation++]);
            }
        }

        Iterator iter = indexMap.entrySet().iterator();
        while(iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            System.out.println(entry.getKey() + " " +entry.getValue());
        }
    }

    public static void main(String[] args) {
        findTwoSum(new int[] { 3, 1, 5, 7, 5, 9 }, 10);
    }
}