package test;

import java.util.ArrayList;

/*
   Question: TestDome - Path
   Solution by Arash Partow 2014

   Write a function that provides change directory (cd) function for an
   abstract file system.

   Notes:

   1. Root path is '/'.
   2. Path separator is '/'.
   3. Parent directory is addressable as "..".
   4. Directory names consist only of English alphabet letters (A-Z and a-z).

   For example, Path("/a/b/c/d").cd("../x").getPath() should return "/a/b/c/x".

   Note: Do not use built-in path-related functions.
*/
public class PathCD {
    private String path;

    public PathCD(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void cd(String newPath) {
        if (newPath.equals("/")) {
            path = "/";
        }

        String[] arrOfStr = newPath.split("/", 0);
        String sb = path;

        for (String a : arrOfStr) {
            if (a.equals("..")) {
                sb = sb.substring(0, sb.lastIndexOf("/"));
            } else {
                sb += "/";
                sb += a;
            }
        }

        path = sb;
    }

    public static void main(String[] args) {
        PathCD path = new PathCD("/a/b/c/d");
        path.cd("x/../z");
        System.out.println(path.getPath());

    }
}
