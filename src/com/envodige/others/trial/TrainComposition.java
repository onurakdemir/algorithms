package test;

import java.util.Deque;
import java.util.LinkedList;

public class TrainComposition {
    Deque<Integer> deque = new LinkedList<Integer>();
    public void attachWagonFromLeft(int wagonId) {
        deque.addFirst(wagonId);
    }

    public void attachWagonFromRight(int wagonId) {
        deque.addLast(wagonId);
    }

    public int detachWagonFromLeft() {
       return deque.removeFirst();
    }

    public int detachWagonFromRight() {
       return deque.removeLast();
    }

    public static void main(String[] args) {
        TrainComposition train = new TrainComposition();
        train.attachWagonFromLeft(7);
        train.attachWagonFromLeft(13);
        System.out.println(train.detachWagonFromRight()); // 7
        System.out.println(train.detachWagonFromLeft()); // 13
    }
}
