package com.envodige;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * This algorithm uses TreeMap data structure to keep intervals sorted.
 *
 * Every single interval is used to create a single entry. First value of the interval is the key
 * and the second value of the interval is the value of the map entry.
 * The next possible interval is found by the value of the previous entry.
 *
 * key -> value -> key -> value ...
 *
 */
class Sequence {

    private static final TreeMap<Float, Float> dic = new TreeMap<>();
    private static final float[][] intervals = {{42, 43}, {2, 4}, {13, 25}, {4, 9}, {41, 42}, {9, 13}, {6, 8}, {40, 41}, {43, 63}, {-1, -11}, {-11, -40}, {-40, -63}};

    public static void main(String[] args) {
        buildTree();

        List<List<Float>> result = process();

        for (List<Float> list : result) {
            for (int i = 0; i < list.size(); i += 2) {
                System.out.print("(" + list.get(i) + " " + list.get(i + 1) + "),");
            }
            System.out.println("  TotalLength -> " + (int) (list.get(list.size() - 1) - list.get(0)));
        }
    }

    /**
     * This method starts with the first entry and checks for a sequence.
     *
     * @return the list of sequences.
     */
    private static List<List<Float>> process() {
        List<List<Float>> result = new ArrayList<>();
        List<Float> visitedKeys = new ArrayList<>();

        List<Float> currIntervals;
        int totalLenght = 0;
        // Check the sequences for every single entry.
        for (Map.Entry<Float, Float> entry : dic.entrySet()) {
            currIntervals = new ArrayList<>();
            currIntervals.add(entry.getKey());
            currIntervals.add(entry.getValue());
            // improve the performance without visiting the same interval again.
            visitedKeys.add(entry.getKey());
            Float currKey = entry.getValue();
            while (currKey != null) {
                if (dic.containsKey(currKey) && !visitedKeys.contains(currKey)) {
                    currIntervals.add(currKey);
                    currIntervals.add(dic.get(currKey));
                    currKey = dic.get(currKey);
                    visitedKeys.add(entry.getKey());
                } else {
                    currKey = null;
                }
            }
            int size = currIntervals.size();
            if (size > 2) {
                int tempTotal = (int) (currIntervals.get(size - 1) - currIntervals.get(0));
                // initial value.
                if (totalLenght == 0) {
                    totalLenght = tempTotal;
                }
                // add the new sequence with the same length
                if (tempTotal == totalLenght) {
                    result.add(currIntervals);
                }
                // clear the existing sequence(s), longer sequence is found.
                if (tempTotal > totalLenght) {
                    totalLenght = tempTotal;
                    result.clear();
                    result.add(currIntervals);
                }
            }
        }
        return result;
    }


    /**
     * Builds a TreeMap which will keep intervals inorder.
     */
    private static void buildTree() {
        for (float[] interval : intervals) {
            dic.put(interval[0], interval[1]);
        }
        for(Map.Entry ent:dic.entrySet()) {
            System.out.println(ent.getValue() + " "+ent.getKey());
        }
    }
}
