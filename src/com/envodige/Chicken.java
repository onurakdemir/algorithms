package com.envodige;

import java.util.concurrent.Callable;

interface Bird {
    Egg lay();
}

class Chicken  implements  Bird{

    public Chicken() {
    }

    public static void main(String[] args) throws Exception {
        Chicken chicken = new Chicken();
        System.out.println(chicken instanceof Bird);
    }

    @Override
    public Egg lay() {
        return new Egg(Chicken::new);
    }
}

class Egg {
    Callable<Bird> createBird;

    public Egg(Callable<Bird> createBird) {
        this.createBird = createBird;
    }

    public Bird hatch() throws Exception {
        if (createBird == null)

            throw new IllegalStateException();

        try {

            return createBird.call();

        } finally {

            createBird = null;

        }
    }
}
