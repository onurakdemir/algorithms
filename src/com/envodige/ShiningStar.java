package com.envodige;

public class ShiningStar {
    double shineFactor = 1d;
    String name;

    public ShiningStar(double shine) {
        shineFactor = shine;
    }

    public double getShineFactor() {
        return shineFactor;
    }

    public void setShineFactor(double shineFactor) {
        this.shineFactor = shineFactor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double shine() {
        if (shineFactor < 0.0) {
            throw new IllegalStateException();
        }
        return shineFactor;
    }

    public void fadeOut() {
        shineFactor = -1.0;
    }

    public static void main(String[] args) {
        // Code for debugging the test case NewStarsCanShine
        // double shineFactor = 1d;
        // ShiningStar star = new ShiningStar(shineFactor);
        // System.out.println(String.format("Are equal: %b, expected: %f, actual: %f", shineFactor == star.shine(), shineFactor, star.shine()));
    }
}