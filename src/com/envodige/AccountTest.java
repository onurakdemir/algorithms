package com.envodige;

import org.junit.Assert;
import org.junit.Test;

public class AccountTest {
    private double epsilon = 1e-6;

    @Test
    public void accountCannotHaveNegativeOverdraftLimit() {
        Account account = new Account(-20);

        Assert.assertEquals(0d, account.getOverdraftLimit(), epsilon);
    }

    @Test
    public void depositAndWithdrawNotNegative(){
        Account account = new Account(20);

        Assert.assertFalse(account.withdraw(-20));
        Assert.assertFalse(account.deposit(-20));
    }

    @Test
    public void accountCannotOverstep(){
        Account account = new Account(20);

        Assert.assertFalse(account.withdraw(30));
    }

    @Test
    public void testWithdrawAndDeposit() {
        Account account = new Account(0);
        account.deposit(40);
        Assert.assertEquals(account.getBalance(), 40, 0.1);
        account.withdraw(20);
        Assert.assertEquals(20, account.getBalance(), 0.1);
    }

    @Test
    public void testWithdrawAndDepositReturn() {
        Account account = new Account(0);
        Assert.assertFalse(account.withdraw(10));

        Assert.assertTrue(account.deposit(10));

        Assert.assertTrue(account.withdraw(5));

    }
}


//    The deposit and withdraw methods will not accept negative numbers.
//        Account cannot overstep its overdraft limit.
//        The deposit and withdraw methods will deposit or withdraw the correct amount, respectively.
//        The withdraw and deposit methods return the correct results.